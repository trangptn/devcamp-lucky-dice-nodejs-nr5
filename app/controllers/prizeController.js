const  prizeModel= require("../models/prizeModel");

const mongoose= require('mongoose');

const createPrize = async (req,res)=>{
    const{
        reqName,
        reqDescription
    }= req.body

    if(!reqName)
    {
        res.status(400).json({
            message:"Name khong hop le"
        })
        return false;
    }
    try{
        var newprize={
            name:reqName,
            description:reqDescription
        }

        const result = await prizeModel.create(newprize);

        res.status(201).json({
            message:"Tao prize thanh cong ",
            data:result
        })
    }
    catch(err)
    {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getAllPrize= async  (req,res) =>{
    try{
        const result = await prizeModel.find();
        return res.status(200).json({
            message:"Lay du lieu thanh cong",
            data:result
        })
    }
    catch(err)
    {
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getPrizeById = async (req,res)=>{
    const prizeid=req.params.prizeid;

    if(!mongoose.Types.ObjectId.isValid(prizeid))
    {
        res.status(400).json({
            message:"Prize Id khong hop le"
        })
    }
    const result= await prizeModel.findById(prizeid);
    try{
        return res.status(200).json({
            message:"Lay du lieu thanh cong",
            data:result
        })
    }
    catch(err)
    {
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const updatePrizeById= async (req,res)=>{
    const prizeid= req.params.prizeid;

    const {reqDescription}=req.body;
    const reqUpdatedAt=Date.now();
    try{
        var newUpdatePrize={};
        if(reqDescription)
        {
            newUpdatePrize.description=reqDescription;
        }
        newUpdatePrize.updatedAt=reqUpdatedAt;
        
        const result= await prizeModel.findByIdAndUpdate(prizeid,newUpdatePrize);

        if(result)
        {
            res.status(200).json({
                message:"Cap nhat thong tin thanh cong",
                data:result
            })
        }
        else
        {
            res.status(400).json({
                message:"Khong tim thay thong tin prize"
            })
        }
    }
    catch(err)
    {
        console.log(err);
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }

}

const deletePrizeById = async (req,res) =>{
    const prizeid=req.params.prizeid;
    try{
        const result = await prizeModel.findByIdAndDelete(prizeid);
        if(result)
        {
            res.status(200).json({
                message:"Xoa thong tin prize thanh cong"
            })
        }
        else{
            res.status(400).json({
                message:"Khong tim thay thong tin prize"
            })
        }
    }
    catch(err)
    {
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}
module.exports={
    createPrize,
    getAllPrize,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
}