const  voucherModel= require("../models/voucherModel");

const mongoose= require('mongoose');

const createVoucher = async (req,res)=>{
    const{
        reqCode,
        reqDiscount,
        reqNote
    }= req.body

    if(!reqCode)
    {
        res.status(400).json({
            message:"Code khong hop le"
        })
        return false;
    }
    if(!reqDiscount || ! Number.isInteger(reqDiscount))
    {
        res.status(400).json({
            message:"Discount khong hop le"
        })
        return false;
    }

    try{
        var newVoucher={
            code:reqCode,
            discount:reqDiscount,
            note:reqNote
        }

        const result = await voucherModel.create(newVoucher);

        res.status(201).json({
            message:"Tao voucher thanh cong ",
            data:result
        })
    }
    catch(err)
    {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getAllVoucher= async  (req,res) =>{
    try{
        const result = await voucherModel.find();
        return res.status(200).json({
            message:"Lay du lieu thanh cong",
            data:result
        })
    }
    catch(err)
    {
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getVoucherById = async (req,res)=>{
    const voucherid=req.params.voucherid;

    if(!mongoose.Types.ObjectId.isValid(voucherid))
    {
        res.status(400).json({
            message:"Voucher Id khong hop le"
        })
    }
    const result= await voucherModel.findById(voucherid);
    try{
        return res.status(200).json({
            message:"Lay du lieu thanh cong",
            data:result
        })
    }
    catch(err)
    {
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const updateVoucherById= async (req,res)=>{
    const voucherid= req.params.voucherid;

    const
     { reqDiscount,
        reqNote
    }=req.body;
    const reqUpdatedAt=Date.now();
    try{
        var newUpdateVoucher={};
        if(reqDiscount)
        {
            newUpdateVoucher.discount=reqDiscount;
        }
        if(reqNote)
        {
            newUpdateVoucher.note=reqNote;
        }
        newUpdateVoucher.updatedAt=reqUpdatedAt;
        
        const result= await voucherModel.findByIdAndUpdate(voucherid,newUpdateVoucher);

        if(result)
        {
            res.status(200).json({
                message:"Cap nhat thong tin thanh cong",
                data:result
            })
        }
        else
        {
            res.status(400).json({
                message:"Khong tim thay thong tin voucher"
            })
        }
    }
    catch(err)
    {
        console.log(err);
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }

}

const deleteVoucherById = async (req,res) =>{
    const voucherid=req.params.voucherid;
    try{
        const result = await voucherModel.findByIdAndDelete(voucherid);
        if(result)
        {
            res.status(200).json({
                message:"Xoa thong tin voucher thanh cong"
            })
        }
        else{
            res.status(400).json({
                message:"Khong tim thay thong tin voucher"
            })
        }
    }
    catch(err)
    {
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}
module.exports={
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}