const express= require('express');

const router= express.Router();

const{
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById,
    voucherHistoryHandle
}=require('../controllers/voucherHistoryController');


router.post("/voucherHistorys",createVoucherHistory);
router.get("/voucherHistorys",getAllVoucherHistory);
router.get("/voucherHistorys/:voucherhistoryid",getVoucherHistoryById);
router.put("/voucherHistorys/:voucherhistoryid",updateVoucherHistoryById);
router.delete("/voucherHistorys/:voucherhistoryid",deleteVoucherHistoryById);
router.get("/devcamp-lucky-dice/voucher-history",voucherHistoryHandle);
module.exports=router;